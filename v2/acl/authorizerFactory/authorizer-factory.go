package authorizerFactory

import (
	"gitlab.com/ptami_lib/api/v2/acl"
	"gitlab.com/ptami_lib/api/v2/acl/cognitoAuthorizer"
	"gitlab.com/ptami_lib/api/v2/acl/dummyAuthorizer"
)

func GetAuthorizer(identity *acl.Identity) (authorizer acl.IAuthorizer) {
	switch identity.Type {
	case acl.IdentityTypeDummy:
		authorizer = dummyAuthorizer.New(*identity)
	case acl.IdentityTypeCognito:
		authorizer = cognitoAuthorizer.New(*identity)
	}

	return
}
