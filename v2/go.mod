module gitlab.com/ptami_lib/api/v2

go 1.14

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/aws/aws-sdk-go-v2/service/cognitoidentityprovider v1.6.2
	gitlab.com/ptami_lib/util v1.0.6
)
