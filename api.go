package api

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/ptami_lib/util"
	"strings"
)

type Request struct {
	Stage   string      `json:"stage"`
	Action  string      `json:"action"`
	Payload interface{} `json:"payload"`
}

type Response struct {
	Error   string      `json:"error"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data"`
	Count   *uint64     `json:"count,omitempty"`
	Start   *uint64     `json:"start,omitempty"`
	Total   *uint64     `json:"total,omitempty"`
}

type ErrorCode struct {
	Code       string `json:"error"`
	Message    string `json:"message"`
	StatusCode int    `json:"statusCode"`
}

func MustBuildPayload(requestProxy events.APIGatewayProxyRequest) (payload interface{}) {
	payloadMap := map[string]interface{}{}

	// body
	if "" != requestProxy.Body {
		_ = json.Unmarshal([]byte(requestProxy.Body), &payloadMap)
	}

	// path
	if nil != requestProxy.PathParameters {
		for k, v := range requestProxy.PathParameters {
			payloadMap[k] = v
		}
	}

	// querystring
	if nil != requestProxy.QueryStringParameters {
		for k, v := range requestProxy.QueryStringParameters {
			payloadMap[k] = v
		}
	}

	payload = payloadMap

	return
}

func (r Response) MustBuildProxyResponse(errorCodes []ErrorCode) (response events.APIGatewayProxyResponse) {
	statusCode := 500
	r.Message = fmt.Sprintf("error code not found (%s)", r.Error)

	for _, errorCode := range errorCodes {
		if 0 == strings.Compare(errorCode.Code, r.Error) {
			statusCode = errorCode.StatusCode
			r.Message = errorCode.Message
		}
	}

	response = events.APIGatewayProxyResponse{
		StatusCode: statusCode,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body:            util.StructToString(&r),
		IsBase64Encoded: false,
	}

	return
}
