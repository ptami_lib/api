module gitlab.com/ptami_lib/api

go 1.14

require (
	github.com/aws/aws-lambda-go v1.26.0
	gitlab.com/ptami_lib/util v1.0.6
)
